import java.io.*;

public class Food2Money2 {

  public static void main(String[] argv)
        throws IOException {

    System.out.println(
        "請選擇：1.炸雞餐 2.漢堡餐 3.起司堡餐 4.薯條餐");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();
    int food = Integer.parseInt(str);

    switch(food){
      case 1:  // 炸雞餐價錢 109 元
        System.out.println("您點的餐點價錢為 109 元");
        break;
      case 2:  // 漢堡餐和起司堡餐同價
      case 3:  // 起司堡餐價錢為 99 元
        System.out.println("您點的餐點價錢為 99 元");
        break;
      case 4:  // 薯條餐價錢為 69 元
        System.out.println("您點的餐點價錢為 69 元");
        break;
      default: // 其它狀況
        System.out.println("您並未點選正確的餐點喔");
    }
  }
}
