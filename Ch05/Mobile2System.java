import java.io.*;

public class Mobile2System {

  public static void main(String[] argv)
        throws IOException {

    System.out.println("請輸入您的行動電話前四碼：");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));

    // 輸入前四碼
    String str = br.readLine();
    int mobile = Integer.parseInt(str);

    // 判斷系統商, 其中 0931 為遠傳、泛亞及東信共同擁有
    if ((mobile == 910) || (mobile == 911) || 
        (mobile == 912) || (mobile == 919) ||
        (mobile == 921) || (mobile == 928) ||
        (mobile == 932) || (mobile == 933) ||
        (mobile == 934) || (mobile == 937) ||
        (mobile == 963) || (mobile == 972))
      System.out.println("中華電信");
    else if ((mobile == 918) || (mobile == 920) || 
        (mobile == 922) || (mobile == 935) ||
        (mobile == 939) || (mobile == 952) ||
        (mobile == 953) || (mobile == 958))
      System.out.println("台灣大哥大");
    else if ((mobile == 916) || (mobile == 917) || 
        (mobile == 926) || (mobile == 930) ||
        (mobile == 936) || (mobile == 955))
      System.out.println("遠傳電信");
    else if ((mobile == 913) || (mobile == 915) || 
        (mobile == 925) || (mobile == 927) ||
        (mobile == 938))
      System.out.println("和信電訊");
    else if ((mobile == 929) || (mobile == 956))
      System.out.println("泛亞電信");
    else if (mobile == 923)
      System.out.println("東信電訊");
    else if (mobile == 931)
      System.out.println("東信電訊、泛亞電信或是遠傳電信");
    else
      System.out.println("抱歉, 無法判定您的系統。");
  }
}
