import java.io.*;

public class CheckOilThree {

  public static void main(String[] argv)
        throws IOException {

    System.out.println("請輸入目前所剩油量 (單位：公升)");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));
    String str = br.readLine();
    
    int liter = Integer.parseInt(str);

    if ((liter >= 2) & (liter < 5)) {
      System.out.println("油量尚足, 提醒您注意油表。");
    }

    System.out.println("祝您行車愉快。");
  }
}
