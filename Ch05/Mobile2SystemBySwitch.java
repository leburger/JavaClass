import java.io.*;

public class Mobile2SystemBySwitch {

  public static void main(String[] argv)
        throws IOException {

    System.out.println("請輸入您的行動電話前四碼：");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));

                // 輸入前四碼
    String str = br.readLine();
    int mobile = Integer.parseInt(str);

    // 判斷系統商, 其中 0931 為遠傳、泛亞及東信共同擁有
    switch(mobile) {
      case 910: case 911: case 912: case 919:
      case 921: case 928: case 932: case 933:
      case 934: case 937: case 963: case 972:
        System.out.println("中華電信");
        break;
      case 918: case 920: case 922: case 935:
      case 939: case 952: case 953: case 958:
        System.out.println("台灣大哥大");
        break;
      case 916: case 917: case 926: case 930:
      case 936: case 955:
        System.out.println("遠傳電信");
        break;
      case 913: case 915: case 925: case 927:
      case 938:
        System.out.println("和信電訊");
        break;
      case 929: case 956:
        System.out.println("泛亞電信");
        break;
      case 923:
        System.out.println("東信電訊");
        break;
      case 931:
        System.out.println("東信電訊、泛亞電信或是遠傳電信");
        break;
      default:
        System.out.println("抱歉, 無法判定您的系統。");
    }
  }
}
