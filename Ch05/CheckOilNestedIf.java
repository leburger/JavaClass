import java.io.*;

public class CheckOilNestedIf {

  public static void main(String[] argv)
        throws IOException {

    System.out.println("請輸入目前所剩油量 (單位：公升)");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();
    int liter = Integer.parseInt(str);

    if (liter >= 2) {  // 第 1 層 if 敘述
      if (liter < 5) { // 第 2 層 if 敘述
        System.out.println("油量尚足, 提醒您注意油表。");
      }
    }

    System.out.println("祝您行車愉快。");
  }
}
