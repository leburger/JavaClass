import java.io.*; // 為輸入資料加上的程式

public class CheckOilWrong {

  public static void main(String[] args) 
        throws IOException { // 為輸入資料加上的程式

    System.out.println("請輸入目前所剩油量 (單位：公升)");
    System.out.print("→");

    // 為輸入資料加上的程式
    BufferedReader br = new 
      BufferedReader(new InputStreamReader(System.in));
    
    String str = br.readLine(); // 取得輸入資料
    
    // 將輸入資料轉成整數
    int liter = Integer.parseInt(str);

    if (liter < 2)
      System.out.println("油量已經不足");
      System.out.println("該加油囉！");

    System.out.println("祝您行車愉快。");
  }
}
