import java.io.*;

public class CheckOilBadIf {

  public static void main(String args[]) 
        throws IOException {

    System.out.println("請輸入目前所剩油量 (單位：公升)");
    System.out.print("→");

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));
    
    String str = br.readLine();    
    int liter = Integer.parseInt(str);

    if (liter >= 10) { // 條件 1
      // 敘述 1
      System.out.println("油量充足, 請安心上路");
    }
    else if (liter < 10) { // 條件 2
      // 敘述 2
      System.out.println("油量尚足, 提醒您注意油表。");
    }
    else if (liter < 2) { // 條件 3    
      // 敘述 3
      System.out.println("油量已經不足, 該去加油了！");
    }

    System.out.println("祝您行車愉快。");
  }
}
