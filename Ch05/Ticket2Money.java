import java.io.*;

public class Ticket2Money {

  public static void main(String[] argv)
        throws IOException {

    BufferedReader br = new 
        BufferedReader(new InputStreamReader(System.in));

    int num = 0; // 張數
    int option = 0; // 票種
    int ticket = 260; // 電影票單價 (預設為全票)

    // 輸入票種
    System.out.println("請輸入您欲選購的電影票種類：");
    System.out.println(
        "1.全票(260) 2.軍警學生票(220) 3.早場票/半票(190)");
    System.out.print("→");
    String str = br.readLine();
    option = Integer.parseInt(str);

    // 輸入張數
    System.out.print("請輸入欲購張數：");
    System.out.print("→");
    str = br.readLine();
    num = Integer.parseInt(str); 

    // 依據票種取得單價
    switch(option){
      case 1: // 全票(260)
        ticket = 260;
        break;
      case 2: // 軍警學生票(220)
        ticket = 220;
        break;
      case 3: // 早場票/半票(190)
        ticket = 190;
        break;
    }

    System.out.println("總價：" + (ticket * num));
  }
}
