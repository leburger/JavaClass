import java.io.*;

public class Prime {

  public static void main(String args[]) throws IOException {

    System.out.println("請輸入尋找的範圍");
    System.out.print("→");

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));
    String str = br.readLine();
    int range = Integer.parseInt(str);

    int count = 0;             // 用來計算共有幾個質數

    for (int i=2;i<=range;i++) {
      boolean isPrime = true;  // 判斷是否為質數的布林值

      for (int j=2;j<i;j++)    // 做除法運算的內迴圈
        if ((i%j) == 0) {      // 餘數為 0 表示可以整除,
          isPrime = false;     // 所以不是質數,
          break;               // 也不用繼續除了
        }

      if (isPrime) {           // 若是質數, 即輸出該數值
         System.out.print(i +"\t");
         count++;
         if ((count%5) == 0)   // 設定每列輸出五個質數
           System.out.println();
      }
    }
    System.out.print("\n小於等於 " +range );
    System.out.println(" 的質數共有 " + count + " 個");
  }
}
