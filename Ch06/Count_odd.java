import java.io.*;

public class Count_odd {

  public static void main(String args[]) throws IOException {

    // 宣告累加值 sum 及計算範圍 range
    int sum = 0, range;

    System.out.println("請輸入欲計算的奇數和範圍 (結尾數值)：");
    System.out.print("→");

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();
    range = Integer.parseInt(str);

    // 由 1 開始, 每次加 2 直到 i 值大於 range 的 for 迴圈
    for (int i=1;i<=range;i+=2) { // 宣告迴圈變數 i
                                  // 每跑一次迴圈就將 i 值加 2
      sum += i;
    }
    System.out.println("1 到 "+range+" 的所有奇數和為 "+sum);
  }
}
