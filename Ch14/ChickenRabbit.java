import java.io.*;

// 宣告自訂的例外類別
class ValueException extends RuntimeException {

  public ValueException (String s) {
    super(s);
  }
}

public class ChickenRabbit {

  public static void main(String[] argv) throws IOException{

    System.out.println("計算雞兔同籠問題");

    int feet=0,heads=0;  // 腳和頭的數目

    do {
      BufferedReader br =
        new BufferedReader(new InputStreamReader(System.in));

      System.out.print("\n請輸入雞兔的總頭數→");
      String str = br.readLine();
      heads = Integer.parseInt(str);

      System.out.print("請輸入雞兔的總腳數→");
      str = br.readLine();
      feet = Integer.parseInt(str);
    } while (feet<0 | heads<0); // 輸入負值時, 會請使用者重新輸入

    int chicken = 0, rabbits = 0;

    try {
      // 若 (腳數 - 頭數*2) 不能被 2 整除, 即拋出例外
      if (((feet - 2*heads) % 2) !=0)
        throw new ValueException("本程式不考慮三腳兔、獨腳雞等狀況！");

      rabbits = (feet - 2*heads) / 2;
      // 若免子的數量為負數, 則拋出例外
      if (rabbits < 0)
        throw new ValueException("本程式不考慮無腳雞兔等狀況！");

      chicken = heads - rabbits;
      System.out.println("雞有 " + chicken + " 隻");
      System.out.println("兔有 " + rabbits + " 隻");
    }
    catch (ValueException e) {
       System.out.println("輸入數值有誤！");
       System.out.println(e);
    }
  }
}