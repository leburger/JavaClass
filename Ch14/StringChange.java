import java.io.*;

public class StringChange {

  public static void main(String[] argv) throws IOException{

    System.out.println("本程式會將字串中的英文字母大小寫互換");
    System.out.print("請輸入您要轉換的字串→");

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();

    do {
      char[] temp = str.toCharArray();  // 將字串轉成字元陣列

      try {
        for (int i=0;i<temp.length;i++)
          if (Character.isLetter(temp[i]) |
              Character.isWhitespace(temp[i]))
             if (Character.isLowerCase(temp[i]) )
               temp[i] = Character.toUpperCase(temp[i]);
             else
               temp[i] = Character.toLowerCase(temp[i]);
          else       // 遇到非英文字母或空白, 即拋出例外
            throw new RuntimeException();
        System.out.println(temp);
      }
      catch (RuntimeException e) {
        System.out.println("字串中只能含英文字母");
      }
      finally {
        System.out.println("\n請輸入另一個字串");
        System.out.print("(輸入 \"bye\" 結束程式)→");
        str = br.readLine();
      }
    } while (!str.equalsIgnoreCase("bye"));
  }
}
