import java.util.*;
class Student implements Comparable {
   int id;       // 學號
   String name;  // 姓名
   Student(int id, String name) 
      { this.id = id; this.name = name; }
   public String toString()
      { return id + "-" + name; }
   public int compareTo(Object o) 
      { return (this.id - ((Student)o).id); }
}

public class StudentSort1 {
   public static void main(String[] args) {
      TreeSet ts = new TreeSet();
      ts.add(new Student(4502, "陳大東"));
      ts.add(new Student(4503, "簡中南"));
      ts.add(new Student(4501, "王小明"));
      System.out.println(ts);
  }
}
