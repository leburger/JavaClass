import java.util.*;

public class StoreBook {
   public static void main(String[] args) {
      List<Book> list  = new ArrayList<Book>();
      list.add(new Book(101, "Java"));  // 將書籍加入列表中
      list.add(new Book(102, "VB  "));
      list.add(new Book(105, "C#  "));
      
      StoreEz<Book> store = new StoreEz<Book>(list); // 建立商店
      store.report();         // 印出存貨
      
      Book t = store.get(1);  // 讀取位置 1 的商品
      System.out.println("賣出：" + t);
      store.sale(t);          // 賣出商品
      store.report();         // 印出存貨
}  }
