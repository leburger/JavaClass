import java.util.*;

interface Salable {  // 定義可讀取價錢的介面
   int getPrice();
}
class Store<T extends Salable> {
   List<T> list;    // 商品列表
   int balance;     // 目前店裡的現金
   
   Store(List<T> list, int balance) {  // 建構時要傳入
      this.list = list;                //   商品列表
      this.balance = balance;          //   及現金
   }
   T get(int index) {
      return list.get(index);
   }
   boolean sale(T t) {     // 賣出商品, 傳回是否成功
      boolean b = list.remove(t);
      if(b) balance += t.getPrice();
      return b;
   }
   void report() {      // 列出商品及現金
      System.out.println("存貨："+list+" 現金："+balance);
   }
}
