import java.util.*;
public class GenericBook2 {
   public static void main(String[] args) {
      TreeSet<Book> b = new TreeSet<Book>();
      TreeSet t = b;
      TreeSet<Book> b2 = newBook(b, "Java");
      System.out.println(t);
   }
   static TreeSet newBook(TreeSet books, String name) {
      if (books.isEmpty())   
         books.add(new Book(1001, name));
      else          
         books.add(new Book(((Book)books.last()).id + 1, name));
      return books;
   }
}