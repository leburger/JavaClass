import java.util.*;

class Tshirt implements Salable {
   String name;
   int price;
   Tshirt(String name, int price) {
      this.name = name;
      this.price = price;
   }
   public int getPrice()
      { return price; }
   public String toString()        // 將內容轉為易讀的字串   
      { return name + "-" + price; }
   public boolean equals(Object o) // 比較是否相等
      { return (o instanceof Tshirt) && name==((Tshirt)o).name; }
}

public class StoreTshirt {
   public static void main(String[] args) {
      List<Tshirt> list  = new ArrayList<Tshirt>();
      list.add(new Tshirt("紅衫", 390));
      list.add(new Tshirt("紫龍", 500));
      list.add(new Tshirt("青花", 250));
      Store<Tshirt> store = new Store<Tshirt>(list, 1000);
      store.report();
      
      Tshirt t = store.get(1);
      System.out.println("賣出：" + t);
      store.sale(t);
      store.report();
}  }