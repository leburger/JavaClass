class Student {
   int id;       // 學號
   String name;  // 姓名
   Student(int id, String name) 
      { this.id = id; this.name = name; }
   public boolean equals(Object o) {
      return (o instanceof Student) &&   // 先檢查 o 的類別
              id ==  ((Student)o).id;    // 用學號比較
   }
   public int hashCode()
      { return id; }               // 用學號做為雜湊碼
   public String toString()
      { return "學號：" + id + ", 姓名：" + name; }
}

public class Student2 {
   public static void main(String[] args) {
      Student[] arr = { new Student(4501, "王小明"),
                        new Student(4502, "陳大東")};
      Student a = new Student(4502, "陳大東");
      
      System.out.println(arr[1].equals(a) + "," + (arr[1]==a));
      System.out.println(arr[1].hashCode() + "," + a.hashCode());
  }
}
