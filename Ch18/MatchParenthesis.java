import java.io.*;
import java.util.*;

public class MatchParenthesis {

  public static void main(String args[]) throws IOException {

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    System.out.println("請輸入一段算式或程式");
    System.out.print("->");
    String str = br.readLine();
    LinkedList match = new LinkedList();

    for (int i=0;i<str.length();i++)   // 讀字串中的每一個字元
      if (str.charAt(i)=='(')
        match.addFirst(new Character('('));
      else if (str.charAt(i)==')')
        try {
          match.removeFirst();
        }
        catch (NoSuchElementException e) {
          System.out.print("左右括號數量不符, 左括號太少");
          return;
        }

    if(match.isEmpty())
      System.out.print("左右括號數量相符");
    else
      System.out.print("左右括號數量不符, 右括號太少");
  }
}
