import java.util.*;

class StoreEz<T> {
   List<T> list;          // 商品列表

   StoreEz(List<T> list)  // 建構時要傳入商品列表
      { this.list = list; }
   T get(int index)       // 傳回指定位置的商品
      { return list.get(index); }
   boolean sale(T t)      // 賣出商品, 傳回是否成功
      { return list.remove(t); }
   void report()          // 列出商品
      { System.out.println("存貨：" + list); }
}