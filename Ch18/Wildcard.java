import java.util.*;
public class Wildcard {
   static ArrayList<?> sortList(ArrayList<?> aList) { 
      Collections.sort((List) aList);
      return aList; 
   }
   public static void main(String[] args) {
      ArrayList<Integer> ai  = new ArrayList<Integer>();
      ai.add(7); ai.add(6); ai.add(5);
      System.out.println( sortList(ai) );
      
      ArrayList<Long> aL  = new ArrayList<Long>();
      aL.add(7L); aL.add(6L); aL.add(5L);
      System.out.println( sortList(aL) );
      
      ArrayList<String> as  = new ArrayList<String>();
      as.add("Java"); as.add("VB"); as.add("C#");
      System.out.println( sortList(as) );
}  }
