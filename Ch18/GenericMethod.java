import java.util.*;
public class GenericMethod {
   static <T> TreeSet<T> newTreeSet(T a, T b) {
      TreeSet<T> ts = new TreeSet<T>();
      ts.add(a); 
      ts.add(b);
      return ts;
   }
   public static void main(String[] args) {
      TreeSet<String> ts = newTreeSet("b", "a"); // 建立字串集合
      System.out.println("ts=" + ts);
      TreeSet<Book> tt = newTreeSet            // 建立 Book 集合
         (new Book(101, "Java"), new Book(102, "VB"));
      System.out.println("tt=" + tt);
   }
}
