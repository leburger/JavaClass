import java.util.*;
public class PolyElement {
   public static void main(String[] args) {
      ArrayList<Number> o  = new ArrayList<Number>();
      o.add(5);     // 加入 Integer 
      o.add(6L);    // 加入 Long 
      o.add(7.89);  // 加入 Double
      for(Number n: o)
         System.out.println(n.getClass());
}  }
