class Student {
   int id;       // 學號
   String name;  // 姓名
   Student(int id, String name) 
      { this.id = id; this.name = name; }
}

public class Student1 {
   public static void main(String[] args) {
      Student[] arr = { new Student(4501, "王小明"),
                        new Student(4502, "陳大東")};
      Student a = new Student(4502, "陳大東");
      
      System.out.println(arr[1].equals(a) + "," + (arr[1]==a));
      System.out.println(arr[1].hashCode() + "," + a.hashCode());
  }
}
