import java.util.*;
public class GenericBook {
   public static void main(String[] args) {
      TreeSet<Book> b = new TreeSet<Book>();
      newBook(b, "Java 2 精解");
      newBook(b, "Visual C#");
      TreeSet<Book> b2 = newBook(b, "VB 入門");
      System.out.println(b2);
   }
   static TreeSet<Book> newBook(TreeSet<Book> books, String name) {
      if (books.isEmpty())    // 如果集合是空的, 書號由 1001 開始
         books.add(new Book(1001, name));
      else                    // 否則將新書號設為最大書號加 1
         books.add(new Book(books.last().id + 1, name));
      return books;
   }
}