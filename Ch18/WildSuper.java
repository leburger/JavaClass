import java.util.*;
public class WildSuper {
   static ArrayList<?> addInteger(ArrayList<? super Integer> aList) { 
   	  aList.add(8);    // 可以加入 Integer 資料了
      return aList; 
   }
   public static void main(String[] args) {
      ArrayList<Integer> ai  = new ArrayList<Integer>();
      ai.add(7); ai.add(6); ai.add(5);
      System.out.println( addInteger(ai) );
      
      ArrayList<Number> aN  = new ArrayList<Number>();
      aN.add(7); aN.add(6); aN.add(5);
      System.out.println( addInteger(aN) );
}  }
