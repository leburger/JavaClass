import java.util.*;
public class NavigatableSet {
   public static void main(String[] args) {
      Integer[] aPrice = { 100, 120, 136, 150, 180, 190, 200 };
      TreeSet tPrice = new TreeSet(Arrays.asList(aPrice));
      
      // 用 Java 5 的語法來取出小於 140 元的最貴項目
      TreeSet subPrice = (TreeSet) tPrice.headSet(140);
      System.out.println("Java 5: " + subPrice.last());
      
      // 用 Java 6 的語法來取出小於 140 元的最貴項目
      System.out.println("Java 6: " + tPrice.lower(140));
} }

