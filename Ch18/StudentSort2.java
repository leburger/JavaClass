import java.util.*;
class Student implements Comparable {
   int id;       // 學號
   String name;  // 姓名
   Student(int id, String name) 
      { this.id = id; this.name = name; }
   public String toString()
      { return id + "-" + name; }
   public int compareTo(Object o) 
      { return (this.id - ((Student)o).id); }
}
class SoByName implements Comparator { // 依姓名遞增排序
   public int compare(Object o1, Object o2) 
      { return ((Student)o1).name.compareTo(((Student)o2).name); }
}
class SoByIdDes implements Comparator { // 依學號遞減排序
   public int compare(Object o1, Object o2)
      { return ((Student)o2).id -((Student)o1).id; }
}
public class StudentSort2 {
   public static void main(String[] args) {
      TreeSet t1 = new TreeSet(new SoByName()); // 依姓名遞增排序
      t1.add(new Student(4502, "陳大東"));
      t1.add(new Student(4503, "簡中南"));
      t1.add(new Student(4501, "王小明"));
      System.out.println(t1);
      TreeSet t2 = new TreeSet(new SoByIdDes()); // 依學號遞減排序
      t2.addAll(t1);
      System.out.println(t2);
  }
}
