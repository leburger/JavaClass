// 以下就是我們所要撰寫的第一個Java程式
public class FirstJavaWithComment {
	public static void main(String[] argv) {
		// 到上面這兩行為止都是固定的程式骨架

		// 以下開始就是我們真正要執行的程式
		System.out.println("這是我的第一個Java程式。");

		// 以下也都是固定的程式骨架
	}
}