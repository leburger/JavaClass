import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class TempConverter {

  JFrame myframe = new JFrame("華氏與攝氏溫度的轉換");
  JTextField result = new JTextField();  // 轉換結果顯示區
  JTextField degree = new JTextField();  // 輸入區
  JButton f2c = new JButton("華氏轉攝氏");
  JButton c2f = new JButton("攝氏轉華氏");

  public static void main(String[] args) {
    TempConverter test = new TempConverter();
  }

  public TempConverter () {
    // 先取得 ContentPane 物件
    Container contentPane = myframe.getContentPane();

    // 將 5 個元件加到 BorderLayout 的五個位置
    contentPane.add(new JLabel("請輸入溫度"),BorderLayout.NORTH);
    contentPane.add(f2c,BorderLayout.EAST);
    contentPane.add(c2f,BorderLayout.WEST);
    contentPane.add(degree,BorderLayout.CENTER);
    contentPane.add(result,BorderLayout.SOUTH);

    // 華氏轉攝氏按鈕的 ActionListener()
    f2c.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          try {
            // 取得輸入區的字串, 並轉成浮點數
            double f = Double.parseDouble(degree.getText());
            // 進行轉換, 並將結果寫到視窗最下方
            result.setText("華氏 " + f + " 度等於攝氏 "
                           + ((f-32)*5/9) +" 度");
          } catch (NumberFormatException ne) {
            degree.setText("");   // 發生例外時清除輸入區內容
          }
        }
      }
    );

    // 攝氏轉華氏按鈕的 ActionListener()
    c2f.addActionListener(
      new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          try {
            // 取得輸入區的字串, 並轉成浮點數
            double c = Double.parseDouble(degree.getText());
            // 進行轉換, 並將結果寫到視窗最下方
            result.setText("攝氏 " + c + " 度等於華氏 "
                           + (c/5*9 + 32) +" 度");
          } catch (NumberFormatException ne) {
            degree.setText("");   // 發生例外時清除輸入區內容
          }
        }
      }
    );

    myframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    myframe.setSize(400,120);
    myframe.setVisible(true);
  }
}
