public class PostIncrement {
	public static void main(String[] argv) {
		int i = 5,j;
		j = (i++) + 5; // 後置遞增
		System.out.println("變數 i 的內容是：" + i);
		System.out.println("變數 j 的內容是：" + j);

		i = 5;
		j = (++i) + 5; // 前置遞增
		System.out.println("變數 i 的內容是：" + i);
		System.out.println("變數 j 的內容是：" + j);
	}
}
