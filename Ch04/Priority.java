public class Priority {
  public static void main(String[] argv) {
    int i;
    i = 1 + 3 * 5 >> 1; // -> 1 + 15 >> 1 -> 16 >> 1-> 8
    System.out.println("變數 i 現在的內容：" + i);
    i = 1 + 2 >> 1;     // -> 3 >> 1 -> 1
    System.out.println("變數 i 現在的內容：" + i);
  }
}
