public class CastingError  {
  public static void main(String[] argv) {
    int i;
    byte b;
    i = 3;
    b = (byte)i;
    System.out.println("變數 i 現在的內容：" + i);
    System.out.println("變數 b 現在的內容：" + b);

    i = 199;
    b = (byte)i;
    System.out.println("變數 i 現在的內容：" + i);
    System.out.println("變數 b 現在的內容：" + b);
  }
}
