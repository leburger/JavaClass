public class BitwiseComplement {
  public static void main(String[] argv) {
    byte i, j;
    i = 2;  // 00000010
    j = -2; // 11111110
    // ~i -> 11111101 -> -3
    // ~j -> 00000001 -> 1
    System.out.println("~i�G" + (~i));
    System.out.println("~j�G" + (~j));
  }
}