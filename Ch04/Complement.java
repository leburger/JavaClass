public class Complement {
	public static void main(String[] argv) {
		boolean isOn = false; // 假設使用 isOn 代表是否有開燈？
		if(!isOn) {
			System.out.println("現在沒有開燈！");
		}
		isOn = !isOn;  // 利用反向運算子將 isOn 的值反向
		if(isOn) {
			System.out.println("現在有開燈！");
		}

	}
}
