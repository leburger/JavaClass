public class BitwiseLogical {
  public static void main(String[] argv) {
    byte i, j;
    i = 2; //  00000010
    j = -2; // 11111110
    // i | j -> 11111110 -> -2
    // i & j -> 00000010 -> 2
    // i ^ j -> 11111100 -> -4
    System.out.println("i | j�G" + (i | j));
    System.out.println("i & j�G" + (i & j));
    System.out.println("i ^ j�G" + (i ^ j));
  }
}