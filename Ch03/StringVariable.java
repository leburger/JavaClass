public class StringVariable  {
  public static void main(String[] argv) {
     String s1,s2,s3;
     s1 = "第一個字串";
     s2 = "這是第二個字串";
     s3 = s1 + s2;
     System.out.println("變數 s1 現在的內容：" + s1);
     System.out.println("變數 s2 現在的內容：" + s2);
     System.out.println("變數 s3 現在的內容：" + s3);
  }
}
