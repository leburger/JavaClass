public class DoubleValue {
  public static void main(String[] argv) {
    double  d;
    d = 3.4;
    System.out.println("變數 d 的內容為：" + d);
    d = .1234;
    System.out.println("變數 d 的內容為：" + d);
    d = 1.3E2;
    System.out.println("變數 d 的內容為：" + d);
    d = 2.0E-3;
    System.out.println("變數 d 的內容為：" + d);
  }
}
