public class IntegerValue {
  public static void main(String[] argv) {
    int i;
    i = 77;
    System.out.println("變數 i 的內容為：" + i);
    i = 0x77; // = 7 * 16 + 7 = 119
    System.out.println("變數 i 的內容為：" + i);
    i = 0X77; // 與上面相同
    System.out.println("變數 i 的內容為：" + i);
    i = 077; // = 7 * 8 + 7 = 63
    System.out.println("變數 i 的內容為：" + i);
  }
}
