public class StringLength  {
  public static void main(String[] argv) {
    String s1,s2,s3;
    s1 = "第一個字串";
    s2 = "這是第二個字串";
    s3 = s1 + s2;
    System.out.println("變數 s1 的長度：" + s1.length());
    System.out.println("變數 s2 的長度：" + s2.length());
    System.out.println("變數 s3 的長度：" + s3.length());
  }
}
