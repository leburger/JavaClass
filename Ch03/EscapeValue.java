public class EscapeValue {
  public static void main(String[] argv) {
    char  ch;
    ch = '\142';   // 8進位142 = 98 = 'b'
    System.out.println("變數 ch 的內容為：" + ch);
    ch = '\u0062'; // 16進位62 = 98 = 'b'
    System.out.println("變數 ch 的內容為：" + ch);
    ch = '\\';     // \
    System.out.println("變數 ch 的內容為：" + ch);
    ch = '\'';     // '
    System.out.println("變數 ch 的內容為：" + ch);
  }
}
