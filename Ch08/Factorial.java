import java.io.*;

class Computer {
  int factorial(int x) {
    if(x == 0) {
      return 1;
    }
    else { // 呼叫自己計算(x-1)!
       return x * factorial(x - 1);
    }
  }
}

public class Factorial {

  public static void main(String[] argv) throws IOException {
    Computer a = new Computer();

    System.out.print("請輸入 x\n->");
    BufferedReader br = new
      BufferedReader(new InputStreamReader(System.in));
    int x = java.lang.Integer.parseInt(br.readLine());

    System.out.println(x + "! = " + a.factorial(x));
  }
}
