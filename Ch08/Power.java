import java.io.*;

class Mathematics {
  int power(int x,int y) {
    if(y == 0) {
      return 1;
    }
    return x * power(x,y - 1);  // 呼叫自己
  }
}

public class Power {

  public static void main(String[] argv) throws IOException{
    Mathematics m = new Mathematics();

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    System.out.print("請輸入 x：");
    int x = Integer.parseInt(br.readLine());
    System.out.print("請輸入 y：");
    int y = Integer.parseInt(br.readLine());

    System.out.println(m.power(x,y));
  }
}
