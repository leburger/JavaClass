class Car {
  double gas; // 載油量
  double eff; // 耗油率

  void printState() {  // 顯示物件狀態的方法
    System.out.print("目前載油量為 ");
    System.out.print(gas + " 公升, ");
    System.out.print("耗油率為每公升跑 ");
    System.out.println(eff + " 公里");
  }

  void move(double distance) {   // 代表汽車行駛的方法
    if (gas >= distance/eff ) {  // 如果油量夠才行駛
      gas -= distance/eff;       // 將載油量減去用掉的油量
      System.out.println("行駛了 " + distance + " 公里");
    }
  }
}

public class DriveCar1 {
  public static void main(String[] argv) {
    Car oldcar = new Car();  // 建立物件

    oldcar.gas = 100;        // 設定 oldcar 的成員變數值
    oldcar.eff = 10;

    System.out.print("老爺車 oldcar ");
    oldcar.move(50);         // 行駛 50 公里
    oldcar.printState();     // 輸出目前狀態
  }
}
