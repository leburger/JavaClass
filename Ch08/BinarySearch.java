import java.io.*;

class Searcher {
  int[] data;
  int item;
  int search(int[] data,int item) {
    this.data = data;
    this.item = item;
    return binarySearch(0,data.length - 1);
  }

  int binarySearch(int low,int high) {
    if(low > high) { // 找不到
            return -1;
    }

    int middle = (low + high) / 2; // 取得中間元素索引

    if(item == data[middle]) { // 找到了
            return middle;
    }
    else if(item > data[middle]) { // 在右半邊
            return binarySearch(middle + 1,high);
    }
    else { // 在左半邊
            return binarySearch(low,middle - 1);
    }
  }
}

public class BinarySearch {

  public static void main(String[] argv) throws IOException {
    Searcher a = new Searcher();

    System.out.print("請輸入要尋找的資料\n->");
    BufferedReader br = new
            BufferedReader(new InputStreamReader(System.in));
    int target = java.lang.Integer.parseInt(br.readLine());

    int i = a.search(new int[] {3,5,7,8,10,34,56},target);
    if(i == -1) {
            System.out.println("找不到 " + target);
    }
    else {
            System.out.println("在第 " + i + "個元素找到 " + target );
    }
  }
}
