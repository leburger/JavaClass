class Car {
  double gas; // 載油量
  double eff; // 耗油率

  void printState() {  // 顯示物件狀態的方法
    System.out.print("目前載油量為 ");
    System.out.print(gas + " 公升, ");
    System.out.print("耗油率為每公升跑 ");
    System.out.println(eff + " 公里");
  }

  double move(double distance) { // 代表汽車行駛的方法
    if (gas >= distance/eff ) {  // 如果油量夠
      gas -= distance/eff;       // 將載油量減去用掉的油量
      return distance;
    }
    else {                       // 油量不足
       distance = gas * eff;     // 將全部的油都用來行駛
       gas = 0;
       return distance;          // 傳回實際行駛里程
    }
  }
}

public class DriveCar2 {
  public static void main(String[] argv) {
    Car oldcar = new Car();  // 建立物件

    oldcar.gas = 100;        // 設定 oldcar 的成員變數值
    oldcar.eff = 10;

    System.out.print("老爺車 oldcar ");
    System.out.println("行駛了 " + oldcar.move(50) + " 公里");
    System.out.println("行駛了 " + oldcar.move(500) + " 公里");
    System.out.println("行駛了 " + oldcar.move(5000) + " 公里");
    oldcar.printState();
  }
}
