class Car {
  double gas; // 載油量
  double eff; // 耗油率
}

public class BuildCar2 {
  public static void main(String[] argv) {
    Car oldcar = new Car();

    oldcar.gas = 100;   // 設定成員變數值
    oldcar.eff = 10;

    System.out.print("老爺車 oldcar 目前載油量為 ");
    System.out.print(oldcar.gas + " 公升, ");
    System.out.print("耗油率為每公升跑 ");
    System.out.println(oldcar.eff + " 公里");
  }
}
