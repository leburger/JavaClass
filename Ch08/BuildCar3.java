class Car {
  double gas; // 載油量
  double eff; // 耗油率

  void printState() {  // 輸出物件狀態的方法
    System.out.print("目前載油量為 ");
    System.out.print(gas + " 公升, ");
    System.out.print("耗油率為每公升跑 ");
    System.out.println(eff + " 公里");
  }
}

public class BuildCar3 {
  public static void main(String[] argv) {
    Car oldcar = new Car();  // 建立物件

    oldcar.gas = 100;        // 設定成員變數值
    oldcar.eff = 10;

    System.out.print("老爺車 oldcar ");
    oldcar.printState();     // 呼叫方法
  }
}
