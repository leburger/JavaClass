class Car {
  double gas; // 載油量
  double eff; // 耗油率

  void printState() {  // 輸出物件狀態的方法
    System.out.print("目前載油量為 ");
    System.out.print(gas + " 公升, ");
    System.out.print("耗油量為每公升跑 ");
    System.out.println(eff + " 公里");
  }
}

public class CarArray {
  public static void main(String[] argv) {
    Car[] ManyCars = new Car[3];     // 建立物件陣列

    for(int i=0;i<ManyCars.length;i++) {
      ManyCars[i] = new Car();       // 建立物件
      ManyCars[i].gas = 100 + i*10;
      ManyCars[i].eff = 10 + i*2.5;
    }

    for(int i=0;i<ManyCars.length;i++) {
      System.out.print((i+1) + "號車");
      ManyCars[i].printState();      // 呼叫方法
    }
  }
}
