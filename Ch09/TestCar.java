class Car {
  private double gas; // 更猳秖
  private double eff; // 猳瞯

  public void printState() {   // 陪ボン篈よ猭
    System.out.print("ヘ玡更猳秖 ");
    System.out.print(gas + " そど, ");
    System.out.print("猳瞯–そど禲 ");
    System.out.println(eff + " そń");
  }

  public void move(double distance) {   // ═ó︽緋よ猭
    if (gas >= distance/eff ) {  // 狦猳秖镑︽緋
      gas -= distance/eff;       // 盢更猳秖搭ノ奔猳秖
      System.out.println("︽緋 " + distance + " そń");
    }
  }

  public Car(double gas, double eff) {
    this.gas=gas;
    this.eff=eff;
  }

  public Car() {
    // ㊣Τ把计セ
    this(50,20);
  }
}

public class TestCar {
  public static void main(String[] argv) {
    Car oldcar = new Car(30,10);
    Car newcar = new Car();

    System.out.print("ρ份ó oldcar ");
    oldcar.move(50);         // ︽緋 50 そń
    oldcar.printState();     // ㊣よ猭

    System.out.print("穝ó newcar ");
    newcar.move(60);
    newcar.printState();
  }
}
