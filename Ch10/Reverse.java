public class Reverse {  
        
  public static void main(String[] argv) {
    //String a = "這是一個測試字串";
    StringBuffer b = new StringBuffer("測試字串");
    System.out.println(b.reverse()); // 輸出：串字試測
    System.out.println(b.reverse()); // 輸出：測試字串 (轉二次就還原了)
  }
}
