import java.io.*;

public class RegExTest {
        
   public static void main(String[] argv) throws IOException {
      BufferedReader br =
         new BufferedReader(new InputStreamReader(System.in));

      String str; // 記錄使用者輸入資料
      String pat; // 記錄樣式

      System.out.print("請輸入樣式：");
      pat = br.readLine(); // 讀取使用者輸入樣式

      System.out.print("請輸入字串：");
      str = br.readLine(); // 讀取使用者輸入樣式

      if(str.matches(pat)) {
        System.out.println("相符");
      } else {
        System.out.println("不相符");
      }
   }
}
