import java.util.*;
public class DateCalendar {
  public static void main(String[] args) {
      
    Date d = new Date();  // 建立日期物件, 並設為目前的日期/時間
    System.out.println("現在時間是 " + d); // 也可寫成 d.toString()
    
    Calendar c = Calendar.getInstance();  // 建立月曆
    c.setTime(d);               // 設定月曆的日期時間
    
    System.out.println("星期幾呢？ " + (c.get(c.DAY_OF_WEEK)-1));
    
    c.add(Calendar.MONTH, 1);   // 將月曆的日期加一個月
    System.out.println("一個月後是 " + c.getTime());
  }
}
