import java.util.regex.*;
public class MatcherFind {
   public static void main(String[] args) {
      // 編譯第一個參數的 regex 樣式, 建構並存入 p 物件
      Pattern p = Pattern.compile(args[0]);
      // 由 p 建立搜尋第二個參數的 Matcher 物件 
      Matcher m = p.matcher(args[1]); 
      
      while (m.find()) // 用迴圈一一找出所有符合的子字串
         System.out.println("找到第 " + m.start() + 
                " 到 " + (m.end()-1) + " 字的 " + m.group());
}  }
