import java.util.*;
import java.text.*;
import static java.util.Locale.*;       // 匯入 Locale 的靜態成員
import static java.text.NumberFormat.*; // 匯入 NumberFormat 的靜態成員

public class NumberFormatShow {
   public static void main(String[] args) {
      Locale[] loc = { TAIWAN, JAPAN, US, FRANCE };
      String[] rem = { "台灣", "日本", "美國", "法國" };

      Double d = 123.45678;
      System.out.print("預設：" + getInstance().format(d));
      System.out.println("\t"+getCurrencyInstance().format(d));

      for(int i=0; i<4; i++) {  // 顯示不同國別的格式化數字與貨幣
         System.out.print(rem[i]+"："+getInstance(loc[i]).format(d));
         System.out.println("\t"+getCurrencyInstance(loc[i]).format(d));
      }

      NumberFormat nf = getInstance();
      System.out.println("\n預設小數 = "+nf.getMaximumFractionDigits());
      nf.setMaximumFractionDigits(5);  // 設定最大 5 位小數
      System.out.println("5 位小數 = "+nf.format(d));
      nf.setParseIntegerOnly(true);    // 設定只解析整數部份
      try { 
         System.out.println("只讀整數 = "+ nf.parse("123.45678")); 
      } catch (ParseException e) { 
         System.out.println("解析錯誤！"); 
      }
   }
}
