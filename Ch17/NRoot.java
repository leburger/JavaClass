import java.io.*;

public class NRoot {

  public static void main(String args[]) throws IOException {

    System.out.println("您要求幾次方根");
    System.out.print("限輸入整數→");

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();
    int n;
    try {
      n = Integer.parseInt(str);
    }
    catch (NumberFormatException e) {
      System.out.println("限輸入整數");
      return;
    }

    System.out.println("您要求什麼數的 " + str +" 次方根");
    System.out.print("(需大於零)→");

    str = br.readLine();
    double y = Double.parseDouble(str);

    System.out.println(y + " 的 " + n + " 次方根為 " +
                       Math.exp(Math.log(Math.abs(y))/n));
  }                                 // 用絕對值方法取正值
}
