import java.util.*;
public class LocaleShow {
   public static void main(String[] args) {
      Locale[] loc = { new Locale("zh", "TW"),   // 台灣
                       new Locale("ja"),         // 日本
                       new Locale("it"),         // 義大利
                       new Locale("it", "CH") }; // 瑞士

      for(Locale c : loc) // 使用系統預設語系(繁體中文)來輸出
         System.out.print(c.getDisplayLanguage() + 
                    "," + c.getDisplayCountry() + "。");
      System.out.println();
      for(Locale c : loc)  // 使用指定的英文來輸出
         System.out.print(c.getDisplayLanguage(Locale.ENGLISH ) + 
                    "," + c.getDisplayCountry(Locale.ENGLISH) + "。");
      // 以日文輸出日文的語系名稱
      System.out.print("\n" + loc[1].getDisplayLanguage(Locale.JAPAN));
  }
}
