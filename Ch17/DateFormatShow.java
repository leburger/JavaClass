import java.util.*;
import java.text.*;
import static java.text.DateFormat.*; // 匯入 DateFormat 的靜態成員

public class DateFormatShow {
   public static void main(String[] args) {
      Date d = new Date();
      DateFormat df = getInstance();      // 使用預設日期時間及語系
      System.out.println( df.format(d) );
      
      int[] sty = { SHORT,MEDIUM,LONG,FULL };
      String[] rem = { "短","中","長","完" };
      
      for(int i=0; i<4; i++) {            // 使用預設語系輸出各種樣式
         DateFormat f = getDateInstance(sty[i]);
         System.out.println( rem[i] + "：" + f.format(d) );
      }
      for(int i=0; i<4; i++) {            // 使用英文輸出各種樣式
         DateFormat f = getDateInstance(sty[i], Locale.ENGLISH);
         System.out.println( rem[i] + "：" + f.format(d) );
      }
  }
}
