import java.util.Scanner;
public class ScannerCut {
   public static void main(String[] args) {
      String s = "";
      
      Scanner sc = new Scanner(args[0]); // 以字串格式解析
      while(sc.hasNext())       
         s += "<" + sc.next() + ">,";
      s += "\n";
      sc = new Scanner(args[0]);     // 以各種資料型別解析
      while(sc.hasNext()) {  
         if (sc.hasNextInt()) 
            s += "i-" + sc.nextInt() + ",";
         else if (sc.hasNextBoolean()) 
            s += "b-" + sc.nextBoolean() + ",";
         else if (sc.hasNextDouble()) 
            s += "d-" + sc.nextDouble() + ",";
         else
            s += "<" + sc.next() + ">,";
      }
      System.out.println(s);
}  }
