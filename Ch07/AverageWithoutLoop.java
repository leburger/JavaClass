public class AverageWithoutLoop {
  public static void main(String[] argv) {
    double[] students = {70, 65, 90, 85, 95};
    double sum = 0;
    double score;

    for(int i = 0;i <  students.length;i++) {
      score = students[i];
      sum += score; // 加總
    }

    double average =  sum / students.length; // 計算平均

    System.out.println("平均成績：" + average);
  }
}
