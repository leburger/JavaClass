public class NewArray  {
  public static void main(String[] argv) {
    int[] a = {20,30,40};

    System.out.print("陣列a的元素：");

    for(int i : a) { // 顯示陣列a的所有元素
      System.out.print("\t" + i);
    }

    a = new int[2]; // 重新配置陣列
    a[0] = 100;
    a[1] = 200;

    System.out.print("\n陣列a的元素：");

    for(int i : a) { // 顯示陣列a的所有元素
      System.out.print("\t" + i);
    }
  }
}
