public class TwoDimArray {
  public static void main(String[] argv) {
    int[][] a; // 宣告一個2維陣列

    a = new int[3][4]; // 配置空間
    System.out.println("a共有 " + a.length + "個元素。");

    for(int i = 0;i< a.length;i++) {
      System.out.println("a[" + i + "] 共有 " +
              a[i].length + "個元素。");
    }
  }
}
