import java.io.*;

public class BinaryLessThan {
  public static void main(String[] argv)
         throws IOException{
    int[] data = {5,9,13,15,17,19,25,30,45}; // 已排序資料

    System.out.println("請輸入要找尋的資料");
    System.out.print("→");

    BufferedReader br =
      new BufferedReader(new InputStreamReader(System.in));

    String str = br.readLine();

    // 轉換為 int
    int target = Integer.parseInt(str);

    int high = data.length - 1; // 範圍區間右邊位置
    int low = 0; // 範圍區間左邊位置
    int middle = 0;
    int times = 0; // 搜尋次數

    while(low <= high) { // 還沒找完
      times++; // 累計次數
      middle = (low + high) / 2; // 找出中間值

      System.out.println("尋找區間：" + low + "(" +
        data[low] +     ").." + high + "(" + data[high] +
        "),中間：" + middle + "(" + data[middle] + ")");

      if(target <= data[middle]) { // 在左半邊
        high = middle - 1;
      } else { // 在右半邊
        low = middle + 1;
      }
    }

    System.out.println("經過 " + times + " 次的尋找");

    if(low > 0) {
      System.out.println("比" + target + "小的最大元素為" +
        data[low - 1] + "在第" + (low - 1) + "個位置");
    } else {
      System.out.println(
        "陣列中沒有比 " + target + " 小的元素");
    }
  }
}
