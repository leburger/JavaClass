

public class EnumSwitch {
  enum CupSize { Small, Medium, Large }  // 定義【內部】列舉型別

  static String getVolumn(CupSize c) {
 	  switch(c) {
 	  	case Small:  return "300cc";
 	  	case Medium: return "500cc";
 	  	case Large:  return "800cc";
 	  }
 	  return "";   // 在方法的最後一定要加 return
 	}
 	
  public static void main(String[] argv) {
	  CupSize c = CupSize.Medium;
 	  System.out.println(getVolumn(c));
 	  System.out.println(getVolumn(CupSize.Large));
  }
}
