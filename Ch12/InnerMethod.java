
public class InnerMethod {
	static String os = "外部成員";
  public static void main(String[] argv) {
  	
  	class Inner {  // 【方法內部類別】
	    final String is = "內部成員";  	
  		void print() { System.out.print(os + "&" + is); }
  	}
  	
  	Inner ir = new Inner();
  	ir.print();
  }
}
