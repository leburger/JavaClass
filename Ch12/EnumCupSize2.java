
enum CupSize { Small, Medium, Large }  // 定義列舉型別

public class EnumCupSize2 {
   public static void main(String[] argv) {
  	
   	  // 印出列舉陣列中的各元素的順序及列舉值
  	  for(CupSize c : CupSize.values()) 
  		   System.out.println(c.ordinal() + ":" + c);
  }
}
