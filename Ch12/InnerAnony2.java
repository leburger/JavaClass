interface Movable {	void move(); }  // 定義父介面

public class InnerAnony2 {
  public static void main(String[] argv) {
  	Movable m = new Movable() { // 定義 Movable 的匿名子類別, 並建立物件
      int x = 0;
  		public void move() { go(); }  // 實做介面的方法
  		void go() { ++x; }   
  	};
  	m.move();  // 正確, 會將 x 加 5
  	//m.x = 10;  // 錯誤, 不可透過父介面的變數來存取非介面的成員
  	//m.go();    // 錯誤, 原因同上
  }
}
