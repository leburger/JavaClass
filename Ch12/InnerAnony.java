
class Face {	 // 父類別
	void smile() { System.out.print(":)"); } 
}
public class InnerAnony {
  public static void main(String[] argv) {
  	Face c = new Face() { // 定義 Face 的匿名子類別, 並建立物件傳回

  		void smile()   // 重複定義父類別中的方法
  		   { System.out.print("^_^"); }
  	};
  	c.smile(); // 利用多型的特性, 執行匿名子類別的 smile() 方法
  }
}
