
enum CupSize {
   Small(300), Medium(500), Large(800); // 列舉值後面有加參數,
                                        // 會呼叫自訂的建構方法
                                        
	 private int cc;                      // 自訂的成員變數
	 CupSize(int cc) { this.cc = cc; }    // 自訂的建構方法
   
   String getCc() { return cc + "cc"; } // 自訂的一般方法
}

public class EnumCupSizeCc {
   public static void main(String[] argv) {
  	
   	  // 印出列舉陣列中的各元素的順序及列舉值
  	  for(CupSize c : CupSize.values()) 
  		   System.out.println(c.ordinal() + ":" + c 
  		                      + "-" + c.getCc());
  }
}
