
enum CupSize {
   Small(300), 
   Medium(500) { 
   	 String getCc() { return "450~550cc"; } 
   }, Large(800);
                                        
	 private int cc;                      // 自訂的成員變數
	 CupSize(int cc) { this.cc = cc; }    // 自訂的建構方法
   
   String getCc() { return cc + "cc"; } // 自訂的一般方法
}

public class EnumCupSizeCc2 {
   public static void main(String[] argv) {
  	
   	  // 印出列舉陣列中的各元素的順序及列舉值
  	  for(CupSize c : CupSize.values()) 
  		   System.out.println(c.ordinal() + ":" + c 
  		                      + "-" + c.getCc());
  }
}
