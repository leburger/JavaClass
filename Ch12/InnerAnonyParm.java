interface Movable {	void move(); }  // 定義父介面

public class InnerAnonyParm {
  static void myMove(Movable m) 
     { m.move(); m.move(); }

  public static void main(String[] argv) {
  	myMove( new Movable() {  // 建立匿名子物件做為參數
  	  int x = 0;
  		public void move() { System.out.print(++x); }  
  	});
  }
}
