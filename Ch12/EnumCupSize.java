
enum CupSize { Small, Medium, Large }  // 定義列舉型別

public class EnumCupSize {
  public static void main(String[] argv) {
    CupSize c;         // 宣告列舉變數 d
    c = CupSize.Small;
    CupSize d = CupSize.Medium;  // 宣告列舉變數並指定初值
  	
    System.out.println(c + "," + d);
    System.out.println(c.Large);
    System.out.println(CupSize.Small);
  }
}
