class Parent { // 父類別
	int i;
	Parent(int i) {
		this.i = i;
	}
}

class Child extends Parent { // 子類別
}

public class Constructor {
	public static void main(String[] argv) {
		Child c = new Child(1); // 產生子類別的物件
	}
}