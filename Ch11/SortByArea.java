class Shape { // 父類別
  String name = "";
  double area() { return 0; }

  Shape(String name) { this.name = name; }  // 建構方法
  
  public String toString() { return name+"-"+area(); } //顯示內容
  	  
  static void sort(Shape[] arr) {  // 公開的靜態排序方法
    quickSort(arr, 0, arr.length-1); 
  }

  // 私用的靜態方法：進行實際的 QuickSort 排序
  private static void quickSort(Shape[] arr, int start, int end) {

    if(start >= end) { return; } // 如果只有一個元素，直接返回

    double mid = arr[(start + end) / 2].area(); // 取得中間元素的面積
    int lt = start;
    int rt = end;
    while(true) { 
      while(arr[lt].area() < mid) lt++;
      while(arr[rt].area() > mid) rt--;

      if(lt < rt) { // 還未相遇
        Shape t = arr[lt]; arr[lt] = arr[rt]; arr[rt] = t; // 交換元素
        lt++; // 往尾端移動
        rt--; // 往前端移動
        }
      else {
      	break;
      }
    }

    quickSort(arr, start, lt-1); // 遞迴排序前段
    quickSort(arr, rt+1, end);   // 遞迴排序後段
  }
}

class Circle extends Shape { // 圓形
  int r; // 半徑
  Circle(String name, int  r) { super(name); this.r = r; }
  double area() { return 3.14 * r * r; }
}

class Square extends Shape { // 正方形
  int side; // 邊長
  Square(String name, int  side) { super(name); this.side = side; }
  double area() { return side * side; }
}

public class SortByArea {
  public static void main(String[] argv) {
    Shape[] a = { new Circle("C1", 5), new Square("S1", 5), 
  	             new Square("S2", 3), new Circle("C2", 2), 
  	             new Square("S3", 4) };
    for(Shape x: a) System.out.print(x.toString() + " "); // 印出陣列內容
    System.out.println();
    
	   Shape.sort(a);  // 呼叫 Shape 類別的靜態方法進行排序
    
    for(Shape x: a) System.out.print(x.toString() + " "); // 印出陣列內容
    System.out.println();
  }
}