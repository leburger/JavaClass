class Point { 
   double x = 0, y = 0; 
   Point(double x, double y) { this.x = x; this.y=y; }
}

class Circle { 
   Point p;  // 圓心
   double r; // 半徑
   Circle (double x, double y, double r) {
      p = new Point(x, y);
      this.r = r;
   }
   double getArea() {
   	  return 3.14 * r * r;
   }
}

class Cylinder {
   Circle c;  // 圓形 
   double h;  // 高度
   Cylinder(double x, double y, double r, double h) {
   	  c = new Circle(x, y, r);
   	  this.h = h;
   }
   double getVolumn() {  // 計算體積
      return 3.14 * c.r * c.r * h;  // 直接存取 Circle 中的成員變數
   }
}

public class CylinderVolumn {
  public static void main(String[] argv) {
  	Cylinder c = new Cylinder(3, 3, 2, 5);
    System.out.println( c.getVolumn() );
  }
}
